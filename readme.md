Data Analytics Assessment – Sandeep Malayalan
I chose spark scala to complete this assignment, tried in IDE, finally went out in spark-shell.
____________________________________________________________________________________

Initially I loaded the csv file into dataframe to make calculation easy.



val data = spark.read
         .format("csv")
         .option("header", "true") //first line in file has headers
         .option("mode", "DROPMALFORMED")
         .load("C:\\Users\\sandeep malayalan\\Downloads\\Dummy-Data.csv")


--------------------------------------------------------------------------------------------------------

then To change the data into dataframes i used spark.implicits._ and made a globaltemp view to query in sql 

df34.describe().show()

import spark.implicits._
data.createOrReplaceTempView("AppID")

---------------------------------------------------------------------------------------------------------

making sure dataframe in a shape

val sqlconversion = spark.sql("SELECT * FROM AppID")
sqlconversion.show()

---------------------------------------------------------------------------------------------------------
Calculation of BMI

val sqlBMIcal = spark.sql("SELECT *,cast(SUBSTRING(CAST(Ht AS VARCHAR(50)), 0, 1) as int)*0.304 as feet,cast(SUBSTRING(CAST(Ht AS VARCHAR(50)),-1) as int)*0.0254 as inches FROM people")


sqlBMIcal .createOrReplaceTempView("Finaldata")
val sqlBMIcal1  = spark.sql("SELECT *,wt/((feet*feet)+(inches*inches)) as bmi FROM Finaldata)

-----------------------------------------------------------------------------------------------------------











Main Query

val calculated_data = sqlBMIcal1.select(col("*"),
      expr("case when Ins_Age between 18 and 39 and (bmi<17.49 or bmi>38.5) and Ins_Gender='Male' then 750 " +
                       "when Ins_Age between 18 and 39 and (bmi<17.49 or bmi>38.5) and Ins_Gender='Female' then 750-(750 * 0.10)" +
                       "when Ins_Age between 40 and 59 and (bmi<18.49 or bmi>38.5) and Ins_Gender='Male' then 1000 " +
                       "when Ins_Age between 40 and 59 and (bmi<18.49 or bmi>38.5) and Ins_Gender='Female' then 1000-(1000 * 0.10) " +
                       "when Ins_Age > 60 and (bmi<18.49 or bmi>38.5) and Ins_Gender='Male' then (2000) " +
                       "when Ins_Age > 60 and (bmi<18.49 or bmi>38.5) and Ins_Gender='Female' then 2000-(2000 * 0.10) " +
                       "else 500 end").alias("quote"))


-----------------------------------------------------------------------------------------------------


Main Query

val calculated_reason = calculated_data.select(col("*"),
      expr("case when Ins_Age between 18 and 39 and (bmi<17.49 or bmi>38.5) then 'Age is between 18 to 39 and BMI is either less than 17.49 or greater than 38.5' " +
                       "when Ins_Age between 40 and 59 and (bmi<18.49 or bmi>38.5) then 'Age is between 40 to 59 and BMI is either less than 18.49 or greater than 38.5' " +
                       "when Ins_Age > 60 and (bmi<18.49 or bmi>38.5) then  'Age is greater than 60 and BMI is either less than 18.49 or greater than 38.5' " +
                       "else 'BMI is in right range' end").alias("Reason"))

-----------------------------------------------------------------------
Result

calculated_reason .createOrReplaceTempView("Result")
calculated_reason.show


Round off were not presented for any values because of not disturbing the integrity of Data



 





